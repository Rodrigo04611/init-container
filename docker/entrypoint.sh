#!/bin/bash

set -e

die () {
        echo "Usage: /app/entrypoint.sh"
        exit 1
}

#Secret manager
#echo "##### Load Environment variables #####"
#if [ -f /files/outfile.json ] ; then
#    echo "##### File found in shared volumen ---> loading #####"
#    for keyval in $(grep -E '": [^\{]' /files/outfile.json | sed -e 's/: /=/' -e "s/\(\,\)$//"); do
#        #echo "export $keyval"
#        eval export $keyval
#    done
#else
#    echo "##### File not found in shared volumen /files/ #####"
#    echo "##### Failed Secret manager #####"
#    exit 1
#fi
