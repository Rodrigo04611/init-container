FROM openjdk:7

WORKDIR /app

COPY ./target/app.jar .
COPY ./docker .

USER root

# USER java
# TODO arreglar dns del cluster
EXPOSE 8080

ENTRYPOINT ["/app/entrypoint.sh"]
